FROM golang:buster

ADD cmd /build

WORKDIR /build

RUN go build -o /usr/local/bin/app serve/main.go

CMD ["/usr/local/bin/app"]